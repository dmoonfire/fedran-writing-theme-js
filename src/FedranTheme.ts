import { ContentArgs, ContentTheme } from "@mfgames-writing/contracts";
import * as liquid from "@mfgames-writing/liquid";
import * as path from "path";

export class FedranTheme extends liquid.LiquidTheme
{
    constructor()
    {
        let templateDirectory = path.join(__dirname, "..", "templates");
        let styleDirectory = path.join(__dirname, "..", "styles");
        let assetsDirectory = path.join(__dirname, "..", "assets");

        super(templateDirectory, styleDirectory, assetsDirectory);
    }

    public getContentTheme(content: ContentArgs): ContentTheme
    {
        // Start with the defaults.
        var results = super.getContentTheme(content);

        // Figure out the defaults for types.
        switch (content.element)
        {
            case "cover":
            case "legal":
            case "title":
            case "bastard":
            case "dedication":
            case "toc":
            case "preface":
                results.styleName = "blank";
                break;
        }

        // Return the results.
        return results;
    }

    public processParagraph(content: ContentArgs, text: string): string
    {
        // If we aren't in the first paragraphs, then don't do anything.
        let firstParagraph = !content.process.fedranParagraph;

        if (content.process.fedranEpigraph && firstParagraph)
        {
            // Flip the flag so we don't do this every paragraph.
            content.process.fedranParagraph = true;

            // Modify the text of the first paragraph.
            text = text.replace(
                /<p>(\W)?(\w)([^\s]*)/,
                "<p class='first'><span class='first-word'><span class='first-letter'>$1$2</span>$3</span>");
        }

        return text;
    }

    public renderEmphasis(_: ContentArgs, text: string): string
    {
        const replacedSpaces = text.replace(/(\s+)/g, "</em>$1<em>");

        return "<em>" + replacedSpaces + "</em>";
    }

    public renderBlockquote(content: ContentArgs, quote: string): string
    {
        // Figure out if this is the epigraph.
        let isEpigraph = !content.process.fedranEpigraph;

        // Epigraphs are simple and they have an em-dash.
        if (isEpigraph && quote.indexOf("—") >= 0)
        {
            // Set the epigraph in the process.
            content.process.fedranEpigraph = true;

            // The only blockquotes we have in the books at this point are epigraphs
            // so we can split them.
            quote = quote.replace(" — ", "</p><p class='attribution'>—");

            // Class the quote itself.
            quote = quote.replace("<p>", "<p class='quote'>");

            // Wrap everything in the epigraph.
            return "<blockquote class='epigraph'>" + quote + "</blockquote>";
        }

        // Handle all other blockquotes.
        return "<blockquote class='blockquote'>" + quote + "</blockquote>";
    }
}
