import * as crypto from "crypto";
import * as fs from "fs";
import * as path from "path";

const algorithm = "aes-256-ctr";

function getFontsDirectory(): string
{
    return path.join(__dirname, "..", "assets", "fonts");
}

function getPassword(): string
{
    const password = process.env.FEDRAN_THEME_PASSWORD;

    if (!password)
    {
        throw new Error("The environment variable FEDRAN_THEME_PASSWORD is not set.");
    }

    return password;
}

export function encryptAssets()
{
    const dir = getFontsDirectory();
    const password = getPassword();
    const files = fs.readdirSync(dir);

    console.log("Processing fonts in " + dir);

    for (const fileIndex in files)
    {
        // Ignore non-font files (which may be the encrypted files).
        const file = files[fileIndex];

        if (!file.match(/\.otf$/))
        {
            continue;
        }

        // Process the encrypted font.
        console.log("  Processing font " + file);

        const fontPath = path.join(dir, file);
        const buffer = fs.readFileSync(fontPath);
        const cipher = crypto.createCipher(algorithm, password);
        const crypted = Buffer.concat([cipher.update(buffer), cipher.final()]);

        fs.writeFileSync(fontPath + ".aes", crypted);
    }
}

export function decryptAssets()
{
    const dir = getFontsDirectory();
    const password = getPassword();
    const files = fs.readdirSync(dir);

    for (const fileIndex in files)
    {
        // Ignore non-font files (which may be the encrypted files).
        const file = files[fileIndex];

        if (!file.match(/\.otf.aes$/))
        {
            continue;
        }

        // Process the encrypted font.
        const fontPath = path.join(dir, file).replace(".aes", "");
        const buffer = fs.readFileSync(fontPath + ".aes");
        const cipher = crypto.createDecipher(algorithm, password);
        const crypted = Buffer.concat([cipher.update(buffer), cipher.final()]);

        fs.writeFileSync(fontPath, crypted);
    }
}
