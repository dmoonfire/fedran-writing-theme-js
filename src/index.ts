import { decryptAssets } from "./encryption";
import { FedranTheme } from "./FedranTheme";

function loadFedranTheme()
{
    // See if we need to decrypt the assets.
    decryptAssets();

    // Create the theme and return it.
    const theme = new FedranTheme();

    return theme;
}

export default loadFedranTheme;
