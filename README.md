# Digital and Print Theme for Fedran

This is the digital and print theme for books generated in the Fedran universe. It follows the standard conventions used in the earlier books to format epigraphs (as blockquotes) and chapter entries.
