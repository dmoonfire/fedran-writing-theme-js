## [2.0.2](https://gitlab.com/fedran/fedran-theme-js/compare/v2.0.1...v2.0.2) (2019-04-15)


### Bug Fixes

* switching to ES6 to handle module loading ([d18d2cf](https://gitlab.com/fedran/fedran-theme-js/commit/d18d2cf))

## [2.0.1](https://gitlab.com/fedran/fedran-theme-js/compare/v2.0.0...v2.0.1) (2019-04-15)


### Bug Fixes

* return the theme the way [@fedran](https://gitlab.com/fedran)/build expects it ([59e5bef](https://gitlab.com/fedran/fedran-theme-js/commit/59e5bef))

# [2.0.0](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.5...v2.0.0) (2019-04-15)


### Features

* **fonts:** added encrypted source fonts for consistent building ([8d294ad](https://gitlab.com/fedran/fedran-theme-js/commit/8d294ad))


### BREAKING CHANGES

* **fonts:** Fonts are no longer based on the environment and any CI jobs need to be modified to
handle.

## [1.2.5](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.4...v1.2.5) (2018-08-26)


### Bug Fixes

* fixing the margin so it matches previous PDF version ([f402bfd](https://gitlab.com/fedran/fedran-theme-js/commit/f402bfd))

## [1.2.4](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.3...v1.2.4) (2018-08-26)


### Bug Fixes

* removed spacing on the first page ([f301e17](https://gitlab.com/fedran/fedran-theme-js/commit/f301e17))

## [1.2.3](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.2...v1.2.3) (2018-08-12)


### Bug Fixes

* moving the word breaking into the em tags ([e1b1ff5](https://gitlab.com/fedran/fedran-theme-js/commit/e1b1ff5))
* removing word breaking to use default rules ([bdc7ca8](https://gitlab.com/fedran/fedran-theme-js/commit/bdc7ca8))

## [1.2.2](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.1...v1.2.2) (2018-08-12)


### Bug Fixes

* added package management ([112c1ff](https://gitlab.com/fedran/fedran-theme-js/commit/112c1ff))
